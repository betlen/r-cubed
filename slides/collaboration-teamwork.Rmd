---
title: "Collaboration and teamwork in research"
subtitle: "Why is reproducibility important when working in teams?"  
author: 
    - "Daniel R. Witte"
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    self_contained: false
    lib_dir: libs
    css: ["xaringan-themer.css", "../resources/custom.css"]
    nature:
      ratio: "16:9"
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)

# Insert references
library(here)
library(RefManageR)
BibOptions(
    check.entries = FALSE,
    bib.style = "numeric",
    cite.style = "numeric",
    style = "markdown",
    max.names = 2
    # hyperlink = FALSE,
)
bib <- ReadBib(here("resources/refs.bib"), check = FALSE)
knitr::opts_chunk$set(echo = FALSE)
```

```{r xaringan-themer, include=FALSE}
library(xaringanthemer)
mono_accent(
    base_color = "#2a2e44",
    header_font_google = google_font("Fira Sans"),
    text_font_google = google_font("Crimson Text"),
    code_font_google = google_font("Source Code Pro")
)
```

## What is collaboration?

-   Multiple people working on the same problem and exchanging ideas
-   People pooling expertise, time and resources to achieve a common goal

For example:

-   Two researchers jointly writing a paper
-   Several labs working on a problem together
-   Multi-site clinical studies in several countries

Collaboration can be organised at different levels:
-   Individuals
-   Departments
-   Institutions
-   Countries

---
## Why collaborate?

-   Sharing and discussing different perspectives and ideas leads to solutions more rapidly

-   Large undertakings require a joint effort: Many hands make light work

-   Teams with the best mix of competences

-   Access to specific material, equipment or know-how

-   Some projects need geographic distribution or capacity of parallel sites

-   Collaborations publish more and get more citations

-   Working together can be more motivating than solitary work


---
## Collaboration is widespread

.center[
```{r, out.height="85%", out.width="85%"}
```
]

.footnote[Image source from [here](https://olihb.com/2011/01/23/map-of-scientific-collaboration-between-researchers/).]

---
## Collaboration is widespread
.center[
```{r, out.height="55%", out.width="55%"}
```
]

.footnote[Image source from [here](https://olihb.com/2011/01/23/map-of-scientific-collaboration-between-researchers/).]

---
## Collaboration is widespread
.center[
```{r, out.height="85%", out.width="85%"}
```
]

.footnote[Image source from [here](https://olihb.com/2011/01/23/map-of-scientific-collaboration-between-researchers/).]

---
## Collaboration is based on agreement

Agreement is needed on:
-   Goals
-   Participants, their roles and responsibilities
-   Timing and deadlines
-   Commitment of time and resources
-   Expectations on quality
-   Data sharing, ownership, access
-   Intellectual property rights
-   Publication of results and authorships

--

==> *How to communicate about these issues*

---
## Collaboration happens at different places in the research workflow

.center[
```{r, out.height="60%", out.width="60%"}
```
]

--

Where in this research workflow can collaborative research methods be relevant?

---

class: center, middle

# Exercise: giving clear instructions

---
## Example: The first computationally reproducible manuscript

.center[
```{r, out.height="70%", out.width="70%"}
```
]

.footnote[Source from https://elifesciences.org/articles/30274]

---
## Questions?

